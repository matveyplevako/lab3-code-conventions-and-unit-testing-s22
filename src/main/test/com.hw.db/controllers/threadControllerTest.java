package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class threadControllerTest {

    @BeforeEach
    void testCreateThread() {
        newThread = new Thread("username", Timestamp.from(Instant.now()), "forum", "message", "slug", "message", 43);
    }

    @Test
    @DisplayName("Check id and slug")
    void testCheckIdSlug() {
        threadController.CheckIdOrSlug("slug");
        THREAD_DAO.verify(Mockito.times(1),() -> ThreadDAO.getThreadBySlug("slug"));
    }


    @Test
    @DisplayName("Create post")
    void testCreatePost() {
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            List<Post> postsThread = Collections.emptyList();
            threadController controller = new threadController();
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(newThread);
            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(postsThread), controller.createPost("slug", postsThread));
        }
    }

    @Test
    @DisplayName("Get post")
    void testGetPost() {
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            List<Post> postsThread = Collections.emptyList();
            threadController controller = new threadController();
            threadMock.when(() -> ThreadDAO.getPosts(newThread.getId(), 100, 1, null, false)).thenReturn(postsThread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(newThread);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(postsThread), controller.Posts(slug, 100, 1, null, false));
        }
    }

    @Test
    @DisplayName("Change thread")
    void testChangeThread() {
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            Thread changeThread = new Thread("username", new Timestamp(0), "forum", "message", "slug", "message1", 100);
            changeThread.setId(0);
            threadController controller = new threadController();
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(changeThread);
            threadMock.when(() -> ThreadDAO.getThreadById(0)).thenReturn(newThread);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(newThread), controller.change("slug", newThread));
        }
    }

    @Test
    @DisplayName("Get thread information")
    void testThreadInfo() {
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(newThread);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(newThread), controller.info("slug"));
        }
    }

    @Test
    @DisplayName("Create vote")
    void testCreateVote() {
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try(MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                User user = new User("username", "email@email.com", "name", "more name");
                Vote vote = new Vote("username", 5);
                threadController controller = new threadController();
                threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(newThread);
                userMock.when(() -> UserDAO.Info("username")).thenReturn(user);
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(newThread), controller.createVote("slug", vote));
            }
        }
    }

}
